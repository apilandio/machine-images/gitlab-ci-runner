# Packer - Gitlab CI Runner

This repository is used to create Gitlab CI runner base images used for Hetzner cloud virtual servers.

## Features

- Supports "Docker in Docker" builds

## Development

Build the image:

```
$ packer build -var='hcloud_api_token=HCLOUD_API_KEY' .
```

Debug the image:

```
$ packer build -debug -var='hcloud_api_token=HCLOUD_API_KEY' .
```
