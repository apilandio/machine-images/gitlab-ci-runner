#!/usr/bin/env bash

#
# This script is called via Terraform provisioner.
#

set -exo pipefail

echo "[PACKER] Starting server deprovisioning ..."

#
# Provisioning code
#

# Shutdown Gitlab runner gracefully
systemctl stop gitlab-runner

# Shutdown Docker gracefully
systemctl stop docker

# Unregister Gitlab runner registered during provisioning
gitlab-runner unregister --all-runners

#
# End provisioning code
#

echo "[PACKER] Deprovisioning done."

exit 0
