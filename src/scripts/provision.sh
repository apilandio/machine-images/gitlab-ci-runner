#!/usr/bin/env bash

#
# This script is called via Terraform provisioner.
#

set -exo pipefail

echo "[PACKER] Starting server provisioning ..."

#
# Provisioning code
#

# Run `hello-world` image as integration test
docker run hello-world

# Register Gitlab runner
if [[ -z "${GITLAB_CI_RUNNER_URL}" ]]; then
  echo "[PACKER] GITLAB_CI_RUNNER_URL is missing or empty. Aborting!"
  exit 1
fi

if [[ -z "${GITLAB_CI_RUNNER_REGISTRATION_TOKEN}" ]]; then
  echo "[PACKER] GITLAB_CI_RUNNER_REGISTRATION_TOKEN is missing or empty. Aborting!"
  exit 1
fi

gitlab-runner register \
  --non-interactive \
  --url "${GITLAB_CI_RUNNER_URL}" \
  --registration-token "${GITLAB_CI_RUNNER_REGISTRATION_TOKEN}" \
  --executor "docker" \
  --docker-image "alpine:latest" \
  --docker-privileged \
  --docker-volumes "/var/run/docker.sock:/var/run/docker.sock" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"

#
# End provisioning code
#

echo "[PACKER] Provisioning done."

exit 0
